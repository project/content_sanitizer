<?php

namespace Drupal\content_sanitizer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Content sanitizer item annotation object.
 *
 * @see \Drupal\content_sanitizer\Plugin\ContentSanitizerManager
 * @see plugin_api
 *
 * @Annotation
 */
class ContentSanitizer extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
