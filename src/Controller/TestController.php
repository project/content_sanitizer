<?php
  namespace Drupal\content_sanitizer\Controller;

  use Drupal\Core\Controller\ControllerBase;

  /**
   * Temporary class for execute drush code.
   */
  class TestController extends ControllerBase {

    /**
     * @return mixed
     */
    public function execute() {

      /** @var \Drupal\content_sanitizer\Sanitizer $sanitizer */
      $sanitizer = \Drupal::service('content_sanitizer.sanitizer');

      // We retrieve the storage definitions for all fields stored in a database.
      // This automatically excludes any computed fields.
      $entities = $sanitizer->getAllEntityFieldDefinitions();

      // Our array with sanitizing operations.
      $sanitize_orders = [];

      foreach($entities as $entity_id => $bundles) {

        /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $entity_storage */
        $entity_storage = \Drupal::service('entity_type.manager')->getStorage($entity_id);

        // Only for file, node, taxonomy_term and user entities.
        if ( $entity_id == 'node' ) {

          foreach ($bundles as $bundle_id => $fields) {

            // Use the storage definitions to create a sanitizing order.
            foreach ($fields as $field_name => $definition) {
              /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $definition */

              // Fields with custom storage have no table info so we skip them.
              if ($definition->hasCustomStorage()) {
                echo "Skipping field {$field_name} as it has custom storage.\n";
                continue;
              }

              if ($definition->isBaseField()) {
                echo "Skipped base field {$field_name} ({$definition->getType()}).\n";
                continue;
              }

              // The type of field determines how we create the sanitized value.
              $field_type = $definition->getType();

              // Every field has a table for the non-revisioned version of the field.
              $revision_table_types = [FALSE];

              // Some also have a field revision table so they require more work.
              if ($entity_storage->getEntityType()->isRevisionable() && $definition->isRevisionable()) {
                $revision_table_types[] = TRUE;
              }

              foreach ($revision_table_types as $revision_table) {
                $table_name = $sanitizer->generateFieldTableName($definition, $revision_table);

                // Create an order to sanitize this table.
                if (!isset($sanitize_orders[$table_name])) {
                  $sanitize_orders[$table_name] = [
                    'field_name' => $field_name,
                    'field_type' => $field_type,
                    'is_revision_table' => $revision_table,
                    'definition' => $definition,
                    'bundles' => []
                  ];
                }

                // Add this bundle as one to be sanitized.
                $sanitize_orders[$table_name]['bundles'][] = $bundle_id;

                // TODO: Remove this debug output.
                echo "-- {$table_name} ({$field_type})\n";
              }
            }
          }
        }

        echo PHP_EOL;

        // We generate the queries now so we can report any missing fields before
        // asking the user to continue.
        $queries = [];
        $unsupported_types = [];

        foreach ($sanitize_orders as $table_name => $order) {
          try {
            $queries[] = $sanitizer->generateSanitizeQuery($table_name, $order);
          }
          catch (\Drupal\entity_sanitizer\UnsupportedFieldTypeException $exception) {
            $field_type = $exception->getFieldType();

            if (empty($unsupported_types[$field_type])) {
              $unsupported_types[$field_type] = [];
            }

            $unsupported_types[$field_type][] = $exception->getFieldName();
          }
        }

        if (!empty($unsupported_types)) {
          echo "------------------------------------------------" . PHP_EOL;
          echo "The following field types were not supported and fields of those types will not be sanitized!" . PHP_EOL;
          foreach ($unsupported_types as $type => $fields) {
            $output_fields = join(", ", $fields);
            echo PHP_EOL . "{$type} ({$output_fields})" . PHP_EOL;
          }
          echo "------------------------------------------------" . PHP_EOL . PHP_EOL;
        }

//        if (!drush_confirm(dt('Do you really want to sanitize all these entities?'))) {
//          return drush_user_abort();
//        }

        foreach ($queries as $query) {
          // If we don't need a query for this order (e.g. entity reference) then we
          // receive NULL instead of a query to execute.
          if (!empty($query)) {
            $query->execute();
          }
        }

        foreach($entities as $entity_id => $fields) {
          foreach($fields as $field_name => $definition) {
            // TODO: Create a query for each field individually to update the value based on the field type

            // TODO: Add a WHERE condition to the UPDATE query to exclude whitelisted bundle/field combinations.

          }
        }

        echo PHP_EOL;
        }

      return array('#markup' => 'Finished the executed.');

    }
  }
