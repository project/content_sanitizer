<?php
/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */
namespace Drupal\content_sanitizer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_sanitizer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_sanitizer.settings');
    $form['cs_user_entity'] = [
      '#type' => 'details',
      '#title' => t('Users'),
      '#open' => TRUE,
    ];
    $form['cs_user_entity']['user'] = [
      '#type' => 'checkbox',
      '#title' => t('All Users'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.settings')->get('user') == 1 ? TRUE : FALSE,
    ];
    $form['cs_texonomy_entity'] = [
      '#type' => 'details',
      '#title' => t('Texonomies'),
      '#open' => TRUE,
    ];
    $form['cs_texonomy_entity']['texonomy'] = [
      '#type' => 'checkbox',
      '#title' => t('All Texonomies'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.settings')->get('texonomy') == 1 ? TRUE : FALSE,
    ];
    $form['cs_node_entity'] = [
      '#type' => 'details',
      '#title' => t('Content'),
      '#description' => t('Here you can select the content type which you want to sanitize.'),
      '#open' => TRUE,
    ];
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    $options = [];
    $default = [];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
      $default[] = $node_type->id();
    }
    $form['cs_node_entity']['content'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => array_filter($config->get('content')),
    );

    $form['#attached']['library'][] = 'content_sanitizer/content_sanitizer.sanitization';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      parent::submitForm($form, $form_state);
      $this->config('content_sanitizer.settings')
          ->set('texonomy', $form_state->getValue('texonomy'))
          ->set('user', $form_state->getValue('user'))
          ->set('content', $form_state->getValue('content', []))
          ->save();

  }
}