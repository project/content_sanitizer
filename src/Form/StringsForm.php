<?php
/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */
namespace Drupal\content_sanitizer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class StringsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_sanitizer.strings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'strings_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_sanitizer.strings');

    $form['cs_email_field'] = [
      '#type' => 'details',
      '#title' => t('Email string'),
      '#open' => TRUE,
    ];
    $form['cs_email_field']['email_part1'] = [
      '#type' => 'textfield',
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('email_part1'),
    ];
    $form['cs_email_field']['email_markup'] = [
      '#type' => 'markup',
      '#markup' => '<div class=markup>@</div>'
    ];
    $form['cs_email_field']['email_part2'] = [
      '#type' => 'textfield',
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('email_part2'),
    ];
    $form['cs_text_long_field'] = [
          '#type' => 'details',
          '#title' => t('long text string'),
          '#open' => TRUE,
    ];
    $form['cs_text_long_field']['longtext'] = [
      '#type' => 'textarea',
      '#maxlength' => 255,
        '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('longtext'),
    ];
/*    $form['text_summary_field'] = [
      '#type' => 'details',
      '#title' => t('Text with summary string'),
      '#open' => TRUE,
    ];
    $form['text_summary_field']['summary'] = [
      '#type' => 'textarea',
      '#maxlength' => 255,
    ];*/
    $form['cs_address_field'] = [
      '#type' => 'details',
      '#title' => t('Address string'),
      '#open' => TRUE,
    ];
    $form['cs_address_field']['postal_code'] = [
      '#type' => 'textfield',
      '#title' => t('Pin code'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('postal_code'),
    ];
    $form['cs_address_field']['administrative_area'] = [
      '#type' => 'textfield',
      '#title' => t('State'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('administrative_area'),
    ];
    $form['cs_address_field']['locality'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
        '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('locality'),
    ];
    $form['cs_address_field']['dependent_locality'] = [
      '#type' => 'textfield',
      '#title' => t('Neighbourhood'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('dependent_locality'),
    ];
    $form['cs_address_field']['address_line1'] = [
       '#type' => 'textfield',
       '#title' => t('Address line 1'),
       '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('address_line1'),
    ];
    $form['cs_address_field']['address_line2'] = [
      '#type' => 'textfield',
      '#title' => t('Address line 2'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('address_line2'),
    ];
    $form['cs_address_field']['organization'] = [
      '#type' => 'textfield',
      '#title' => t('Company'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('organization'),
    ];
    $form['cs_address_field']['given_name'] = [
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('given_name'),
    ];
    $form['cs_address_field']['additional_name'] = [
      '#type' => 'textfield',
      '#title' => t('Middle name'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('additional_name'),
    ];
    $form['cs_address_field']['family_name'] = [
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#default_value' => \Drupal::configFactory()->getEditable('content_sanitizer.strings')->get('family_name'),
    ];
    $form['cs_image_field'] = [
      '#type' => 'details',
      '#title' => t('Image'),
      '#open' => TRUE,
    ];
    $form['cs_image_field']['image'] = array(
      '#title' => t('Choose Image'),
      '#type' => 'managed_file',
      '#upload_validators' => array('file_validate_extensions' => array('jpeg jpg png gif')),
      '#upload_location' => 'public://',
    /*  '#default_value' => $entity->get('File')->value,*/
      '#progress_indicator' => 'throbber',
      '#status' => FILE_STATUS_PERMANENT,
      '#progress_message' => 'One moment while we save your file...',
    );

      $form['#attached']['library'][] = 'content_sanitizer/content_sanitizer.sanitization';
      return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      parent::submitForm($form, $form_state);
      \Drupal::configFactory()->getEditable('content_sanitizer.strings')
          ->set('email_part1', $form_state->getValue('email_part1'))
          ->set('email_part2', $form_state->getValue('email_part2'))
          ->set('longtext', $form_state->getValue('longtext'))
          ->set('postal_code', $form_state->getValue('postal_code'))
          ->set('administrative_area', $form_state->getValue('administrative_area'))
          ->set('locality', $form_state->getValue('locality'))
          ->set('dependent_locality', $form_state->getValue('dependent_locality'))
          ->set('address_line1', $form_state->getValue('address_line1'))
          ->set('address_line2', $form_state->getValue('address_line2'))
          ->set('organization', $form_state->getValue('organization'))
          ->set('given_name', $form_state->getValue('given_name'))
          ->set('additional_name', $form_state->getValue('additional_name'))
          ->set('family_name', $form_state->getValue('family_name'))
          ->save();

  }
}