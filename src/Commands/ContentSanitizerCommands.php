<?php

namespace Drupal\content_sanitizer\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class ContentSanitizerCommands extends DrushCommands {
    /**
     *
     * @command content-sanitize
     * @aliases cnst
     *
     */
    public function getMessage() {
      $this->output()->writeln('Hello welcome to sanitization module');
      /** @var \Drupal\content_sanitizer\Sanitizer $sanitizer */
      /*$sanitize = \Drupal::service('content_sanitizer.sanitization');*/

      // We retrieve the simple welcome message from service.
    /*  $message = $sanitize->processMessage();
      drush_print($message)*/;
    }
}
