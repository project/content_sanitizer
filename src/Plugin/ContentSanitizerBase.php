<?php

namespace Drupal\content_sanitizer\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Content sanitizer plugins.
 */
abstract class ContentSanitizerBase extends PluginBase implements ContentSanitizerInterface {

  /**
   * {@inheritdoc}
   */
  public function getFieldValues($table_name, $field_name, $columns) {
    return [];
  }

}
