<?php

  namespace Drupal\content_sanitizer\Plugin\ContentSanitizer;

  use Drupal\content_sanitizer\ContentSanitizerBase;

/**
 * Handles sanitizing for the string field types.
 *
 * @package Drupal\content_sanitizer\Plugin\ContentSanitizer;
 *
 * @FieldSanitizer(
 *   id = "string",
 *   label = @Translation("Sanitizer for string type fields")
 * )
 */
class StringSanitizer extends ContentSanitizerBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldValues($table_name, $field_name, $columns) {
    $fields = [
      $field_name . '_value' => "CONCAT_WS(' ', 'Sanitized ', {$table_name}.bundle, '{$field_name} field of type {$this->getPluginId()}', {$table_name}.entity_id, {$table_name}.revision_id, {$table_name}.langcode, {$table_name}.delta)",
    ];

    // Ensure we don't break fields with a max length.
    if (!empty($columns['value']['length']) && is_numeric($columns['value']['length'])) {
      $fields[$field_name . '_value'] = 'SUBSTRING(' . $fields[$field_name . '_value'] . ', 1, ' . $columns['value']['length'] . ')';
    }

    return $fields;
  }
}
