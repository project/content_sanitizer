<?php

namespace Drupal\content_sanitizer\Plugin\ContentSanitizer;

use Drupal\content_sanitizer\ContentSanitizerBase;

/**
 * Handles sanitizing for the email field types.
 *
 * @package Drupal\content_sanitizer\Plugin\ContentSanitizer
 *
 * @FieldSanitizer(
 *   id = "email",
 *   label = @Translation("Sanitizer for email type fields")
 * )
 */
class EmailSanitizer extends ContentSanitizerBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldValues($table_name, $field_name, $columns) {
    $fields = [
      $field_name . '_value' => "CONCAT({$table_name}.bundle, '+', {$table_name}.entity_id, '-', {$table_name}.revision_id, {$table_name}.langcode, {$table_name}.delta, '@{$field_name}.com')",
    ];

    return $fields;
  }
}
