<?php

  namespace Drupal\content_sanitizer\Plugin\ContentSanitizer;

  use Drupal\content_sanitizer\ContentSanitizerBase;

/**
 * Handles sanitizing for the string_long field types.
 *
 * @package Drupal\content_sanitizer\Plugin\ContentSanitizer;
 *
 * @FieldSanitizer(
 *   id = "string_long",
 *   label = @Translation("Sanitizer for string_long type fields")
 * )
 */
class StringLongSanitizer extends ContentSanitizerBase {}
