<?php

  namespace Drupal\content_sanitizer\Plugin\ContentSanitizer;

  use Drupal\content_sanitizer\ContentSanitizerBase;

/**
 * Handles sanitizing for the text_long field types.
 *
 * @package Drupal\content_sanitizer\Plugin\ContentSanitizer
 *
 * @FieldSanitizer(
 *   id = "text_long",
 *   label = @Translation("Sanitizer for text_long type fields")
 * )
 */
class TextLongSanitizer extends ContentSanitizerBase {}
