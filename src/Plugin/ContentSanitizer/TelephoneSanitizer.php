<?php

  namespace Drupal\content_sanitizer\Plugin\ContentSanitizer;

  use Drupal\content_sanitizer\ContentSanitizerBase;

/**
 * Handles sanitizing for the telephone field types.
 *
 * For telephone numbers we just use a default placeholder.
 *
 * @package Drupal\content_sanitizer\Plugin\ContentSanitizer;
 *
 * @FieldSanitizer(
 *   id = "telephone",
 *   label = @Translation("Sanitizer for telephone type fields")
 * )
 */
class TelephoneSanitizer extends ContentSanitizerBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldValues($table_name, $field_name, $columns) {
    $fields = [
      $field_name . '_value' => "'555 1234567'",
    ];

    return $fields;
  }
}
